# Generated from main/bkit/parser/BKIT.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2G")
        buf.write("\u01ca\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3")
        buf.write("\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3")
        buf.write("\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22")
        buf.write("\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25")
        buf.write("\3\26\3\26\3\27\3\27\3\30\3\30\3\30\3\31\3\31\3\31\3\32")
        buf.write("\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\35\3\35")
        buf.write("\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3")
        buf.write("!\3!\3\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%\3%\3%\3")
        buf.write("&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3)")
        buf.write("\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3+\3+\3+\3+\3+\3+\3")
        buf.write("+\3,\3,\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3")
        buf.write("-\3-\3.\3.\3.\3.\3.\3.\3/\3/\3/\3/\3/\3/\3/\3/\3/\3\60")
        buf.write("\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\61\3\61\3\61\3\61")
        buf.write("\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63")
        buf.write("\3\63\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\66")
        buf.write("\3\66\3\66\3\66\3\66\3\66\3\67\3\67\3\67\3\67\3\67\3\67")
        buf.write("\3\67\3\67\3\67\38\38\38\38\38\39\69\u0167\n9\r9\169\u0168")
        buf.write("\3:\3:\3:\3;\3;\3;\3<\3<\3<\3<\7<\u0175\n<\f<\16<\u0178")
        buf.write("\13<\3<\3<\3=\3=\3=\6=\u017f\n=\r=\16=\u0180\3>\3>\3>")
        buf.write("\6>\u0186\n>\r>\16>\u0187\3?\7?\u018b\n?\f?\16?\u018e")
        buf.write("\13?\3?\5?\u0191\n?\3?\6?\u0194\n?\r?\16?\u0195\3?\3?")
        buf.write("\5?\u019a\n?\3?\6?\u019d\n?\r?\16?\u019e\5?\u01a1\n?\3")
        buf.write("@\6@\u01a4\n@\r@\16@\u01a5\3@\7@\u01a9\n@\f@\16@\u01ac")
        buf.write("\13@\3A\6A\u01af\nA\rA\16A\u01b0\3A\3A\3B\3B\3B\3B\7B")
        buf.write("\u01b9\nB\fB\16B\u01bc\13B\3B\3B\3B\3B\3B\3C\3C\3D\3D")
        buf.write("\3E\3E\3F\3F\3\u01ba\2G\3\3\5\4\7\5\t\6\13\7\r\b\17\t")
        buf.write("\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23")
        buf.write("%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36")
        buf.write(";\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63")
        buf.write("e\64g\65i\66k\67m8o9q:s;u<w=y>{?}@\177A\u0081B\u0083C")
        buf.write("\u0085D\u0087E\u0089F\u008bG\3\2\17\3\2\62;\t\2))^^dd")
        buf.write("hhppttvv\5\2$$))^^\4\2ZZzz\4\2\62;CH\4\2QQqq\3\2\629\3")
        buf.write("\2\60\60\4\2GGgg\4\2--//\3\2c|\6\2\62;C\\aac|\5\2\13\f")
        buf.write("\17\17\"\"\2\u01d9\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2")
        buf.write("\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3")
        buf.write("\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2")
        buf.write("\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2")
        buf.write("\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2")
        buf.write("\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2")
        buf.write("\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3")
        buf.write("\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q")
        buf.write("\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2")
        buf.write("[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2")
        buf.write("\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2")
        buf.write("\2\2o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2")
        buf.write("\2\2\2y\3\2\2\2\2{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\2")
        buf.write("\u0081\3\2\2\2\2\u0083\3\2\2\2\2\u0085\3\2\2\2\2\u0087")
        buf.write("\3\2\2\2\2\u0089\3\2\2\2\2\u008b\3\2\2\2\3\u008d\3\2\2")
        buf.write("\2\5\u008f\3\2\2\2\7\u0091\3\2\2\2\t\u0093\3\2\2\2\13")
        buf.write("\u0095\3\2\2\2\r\u0097\3\2\2\2\17\u0099\3\2\2\2\21\u009b")
        buf.write("\3\2\2\2\23\u009d\3\2\2\2\25\u009f\3\2\2\2\27\u00a1\3")
        buf.write("\2\2\2\31\u00a3\3\2\2\2\33\u00a5\3\2\2\2\35\u00a7\3\2")
        buf.write("\2\2\37\u00a9\3\2\2\2!\u00ab\3\2\2\2#\u00ad\3\2\2\2%\u00b0")
        buf.write("\3\2\2\2\'\u00b3\3\2\2\2)\u00b6\3\2\2\2+\u00b9\3\2\2\2")
        buf.write("-\u00bb\3\2\2\2/\u00bd\3\2\2\2\61\u00c0\3\2\2\2\63\u00c3")
        buf.write("\3\2\2\2\65\u00c6\3\2\2\2\67\u00c9\3\2\2\29\u00cd\3\2")
        buf.write("\2\2;\u00d0\3\2\2\2=\u00d3\3\2\2\2?\u00d7\3\2\2\2A\u00db")
        buf.write("\3\2\2\2C\u00dd\3\2\2\2E\u00e0\3\2\2\2G\u00e3\3\2\2\2")
        buf.write("I\u00e7\3\2\2\2K\u00ec\3\2\2\2M\u00f1\3\2\2\2O\u00f8\3")
        buf.write("\2\2\2Q\u00fb\3\2\2\2S\u0101\3\2\2\2U\u0107\3\2\2\2W\u010e")
        buf.write("\3\2\2\2Y\u0117\3\2\2\2[\u0121\3\2\2\2]\u0127\3\2\2\2")
        buf.write("_\u0130\3\2\2\2a\u0138\3\2\2\2c\u013c\3\2\2\2e\u0143\3")
        buf.write("\2\2\2g\u0148\3\2\2\2i\u014e\3\2\2\2k\u0151\3\2\2\2m\u0157")
        buf.write("\3\2\2\2o\u0160\3\2\2\2q\u0166\3\2\2\2s\u016a\3\2\2\2")
        buf.write("u\u016d\3\2\2\2w\u0170\3\2\2\2y\u017b\3\2\2\2{\u0182\3")
        buf.write("\2\2\2}\u018c\3\2\2\2\177\u01a3\3\2\2\2\u0081\u01ae\3")
        buf.write("\2\2\2\u0083\u01b4\3\2\2\2\u0085\u01c2\3\2\2\2\u0087\u01c4")
        buf.write("\3\2\2\2\u0089\u01c6\3\2\2\2\u008b\u01c8\3\2\2\2\u008d")
        buf.write("\u008e\7=\2\2\u008e\4\3\2\2\2\u008f\u0090\7<\2\2\u0090")
        buf.write("\6\3\2\2\2\u0091\u0092\7*\2\2\u0092\b\3\2\2\2\u0093\u0094")
        buf.write("\7+\2\2\u0094\n\3\2\2\2\u0095\u0096\7}\2\2\u0096\f\3\2")
        buf.write("\2\2\u0097\u0098\7\177\2\2\u0098\16\3\2\2\2\u0099\u009a")
        buf.write("\7]\2\2\u009a\20\3\2\2\2\u009b\u009c\7_\2\2\u009c\22\3")
        buf.write("\2\2\2\u009d\u009e\7\60\2\2\u009e\24\3\2\2\2\u009f\u00a0")
        buf.write("\7.\2\2\u00a0\26\3\2\2\2\u00a1\u00a2\7?\2\2\u00a2\30\3")
        buf.write("\2\2\2\u00a3\u00a4\7-\2\2\u00a4\32\3\2\2\2\u00a5\u00a6")
        buf.write("\7/\2\2\u00a6\34\3\2\2\2\u00a7\u00a8\7,\2\2\u00a8\36\3")
        buf.write("\2\2\2\u00a9\u00aa\7\'\2\2\u00aa \3\2\2\2\u00ab\u00ac")
        buf.write("\7^\2\2\u00ac\"\3\2\2\2\u00ad\u00ae\7?\2\2\u00ae\u00af")
        buf.write("\7?\2\2\u00af$\3\2\2\2\u00b0\u00b1\7#\2\2\u00b1\u00b2")
        buf.write("\7?\2\2\u00b2&\3\2\2\2\u00b3\u00b4\7>\2\2\u00b4\u00b5")
        buf.write("\7?\2\2\u00b5(\3\2\2\2\u00b6\u00b7\7@\2\2\u00b7\u00b8")
        buf.write("\7?\2\2\u00b8*\3\2\2\2\u00b9\u00ba\7>\2\2\u00ba,\3\2\2")
        buf.write("\2\u00bb\u00bc\7@\2\2\u00bc.\3\2\2\2\u00bd\u00be\7-\2")
        buf.write("\2\u00be\u00bf\7\60\2\2\u00bf\60\3\2\2\2\u00c0\u00c1\7")
        buf.write("/\2\2\u00c1\u00c2\7\60\2\2\u00c2\62\3\2\2\2\u00c3\u00c4")
        buf.write("\7,\2\2\u00c4\u00c5\7\60\2\2\u00c5\64\3\2\2\2\u00c6\u00c7")
        buf.write("\7^\2\2\u00c7\u00c8\7\60\2\2\u00c8\66\3\2\2\2\u00c9\u00ca")
        buf.write("\7?\2\2\u00ca\u00cb\7\61\2\2\u00cb\u00cc\7?\2\2\u00cc")
        buf.write("8\3\2\2\2\u00cd\u00ce\7@\2\2\u00ce\u00cf\7\60\2\2\u00cf")
        buf.write(":\3\2\2\2\u00d0\u00d1\7>\2\2\u00d1\u00d2\7\60\2\2\u00d2")
        buf.write("<\3\2\2\2\u00d3\u00d4\7@\2\2\u00d4\u00d5\7?\2\2\u00d5")
        buf.write("\u00d6\7\60\2\2\u00d6>\3\2\2\2\u00d7\u00d8\7>\2\2\u00d8")
        buf.write("\u00d9\7?\2\2\u00d9\u00da\7\60\2\2\u00da@\3\2\2\2\u00db")
        buf.write("\u00dc\7#\2\2\u00dcB\3\2\2\2\u00dd\u00de\7(\2\2\u00de")
        buf.write("\u00df\7(\2\2\u00dfD\3\2\2\2\u00e0\u00e1\7~\2\2\u00e1")
        buf.write("\u00e2\7~\2\2\u00e2F\3\2\2\2\u00e3\u00e4\7X\2\2\u00e4")
        buf.write("\u00e5\7c\2\2\u00e5\u00e6\7t\2\2\u00e6H\3\2\2\2\u00e7")
        buf.write("\u00e8\7D\2\2\u00e8\u00e9\7q\2\2\u00e9\u00ea\7f\2\2\u00ea")
        buf.write("\u00eb\7{\2\2\u00ebJ\3\2\2\2\u00ec\u00ed\7G\2\2\u00ed")
        buf.write("\u00ee\7n\2\2\u00ee\u00ef\7u\2\2\u00ef\u00f0\7g\2\2\u00f0")
        buf.write("L\3\2\2\2\u00f1\u00f2\7G\2\2\u00f2\u00f3\7p\2\2\u00f3")
        buf.write("\u00f4\7f\2\2\u00f4\u00f5\7H\2\2\u00f5\u00f6\7q\2\2\u00f6")
        buf.write("\u00f7\7t\2\2\u00f7N\3\2\2\2\u00f8\u00f9\7K\2\2\u00f9")
        buf.write("\u00fa\7h\2\2\u00faP\3\2\2\2\u00fb\u00fc\7G\2\2\u00fc")
        buf.write("\u00fd\7p\2\2\u00fd\u00fe\7f\2\2\u00fe\u00ff\7F\2\2\u00ff")
        buf.write("\u0100\7q\2\2\u0100R\3\2\2\2\u0101\u0102\7D\2\2\u0102")
        buf.write("\u0103\7t\2\2\u0103\u0104\7g\2\2\u0104\u0105\7c\2\2\u0105")
        buf.write("\u0106\7m\2\2\u0106T\3\2\2\2\u0107\u0108\7G\2\2\u0108")
        buf.write("\u0109\7n\2\2\u0109\u010a\7u\2\2\u010a\u010b\7g\2\2\u010b")
        buf.write("\u010c\7K\2\2\u010c\u010d\7h\2\2\u010dV\3\2\2\2\u010e")
        buf.write("\u010f\7G\2\2\u010f\u0110\7p\2\2\u0110\u0111\7f\2\2\u0111")
        buf.write("\u0112\7Y\2\2\u0112\u0113\7j\2\2\u0113\u0114\7k\2\2\u0114")
        buf.write("\u0115\7n\2\2\u0115\u0116\7g\2\2\u0116X\3\2\2\2\u0117")
        buf.write("\u0118\7R\2\2\u0118\u0119\7c\2\2\u0119\u011a\7t\2\2\u011a")
        buf.write("\u011b\7c\2\2\u011b\u011c\7o\2\2\u011c\u011d\7g\2\2\u011d")
        buf.write("\u011e\7v\2\2\u011e\u011f\7g\2\2\u011f\u0120\7t\2\2\u0120")
        buf.write("Z\3\2\2\2\u0121\u0122\7Y\2\2\u0122\u0123\7j\2\2\u0123")
        buf.write("\u0124\7k\2\2\u0124\u0125\7n\2\2\u0125\u0126\7g\2\2\u0126")
        buf.write("\\\3\2\2\2\u0127\u0128\7E\2\2\u0128\u0129\7q\2\2\u0129")
        buf.write("\u012a\7p\2\2\u012a\u012b\7v\2\2\u012b\u012c\7k\2\2\u012c")
        buf.write("\u012d\7p\2\2\u012d\u012e\7w\2\2\u012e\u012f\7g\2\2\u012f")
        buf.write("^\3\2\2\2\u0130\u0131\7G\2\2\u0131\u0132\7p\2\2\u0132")
        buf.write("\u0133\7f\2\2\u0133\u0134\7D\2\2\u0134\u0135\7q\2\2\u0135")
        buf.write("\u0136\7f\2\2\u0136\u0137\7{\2\2\u0137`\3\2\2\2\u0138")
        buf.write("\u0139\7H\2\2\u0139\u013a\7q\2\2\u013a\u013b\7t\2\2\u013b")
        buf.write("b\3\2\2\2\u013c\u013d\7T\2\2\u013d\u013e\7g\2\2\u013e")
        buf.write("\u013f\7v\2\2\u013f\u0140\7w\2\2\u0140\u0141\7t\2\2\u0141")
        buf.write("\u0142\7p\2\2\u0142d\3\2\2\2\u0143\u0144\7V\2\2\u0144")
        buf.write("\u0145\7t\2\2\u0145\u0146\7w\2\2\u0146\u0147\7g\2\2\u0147")
        buf.write("f\3\2\2\2\u0148\u0149\7H\2\2\u0149\u014a\7c\2\2\u014a")
        buf.write("\u014b\7n\2\2\u014b\u014c\7u\2\2\u014c\u014d\7g\2\2\u014d")
        buf.write("h\3\2\2\2\u014e\u014f\7F\2\2\u014f\u0150\7q\2\2\u0150")
        buf.write("j\3\2\2\2\u0151\u0152\7G\2\2\u0152\u0153\7p\2\2\u0153")
        buf.write("\u0154\7f\2\2\u0154\u0155\7K\2\2\u0155\u0156\7h\2\2\u0156")
        buf.write("l\3\2\2\2\u0157\u0158\7H\2\2\u0158\u0159\7w\2\2\u0159")
        buf.write("\u015a\7p\2\2\u015a\u015b\7e\2\2\u015b\u015c\7v\2\2\u015c")
        buf.write("\u015d\7k\2\2\u015d\u015e\7q\2\2\u015e\u015f\7p\2\2\u015f")
        buf.write("n\3\2\2\2\u0160\u0161\7V\2\2\u0161\u0162\7j\2\2\u0162")
        buf.write("\u0163\7g\2\2\u0163\u0164\7p\2\2\u0164p\3\2\2\2\u0165")
        buf.write("\u0167\t\2\2\2\u0166\u0165\3\2\2\2\u0167\u0168\3\2\2\2")
        buf.write("\u0168\u0166\3\2\2\2\u0168\u0169\3\2\2\2\u0169r\3\2\2")
        buf.write("\2\u016a\u016b\7^\2\2\u016b\u016c\t\3\2\2\u016ct\3\2\2")
        buf.write("\2\u016d\u016e\7)\2\2\u016e\u016f\7$\2\2\u016fv\3\2\2")
        buf.write("\2\u0170\u0176\7$\2\2\u0171\u0175\5s:\2\u0172\u0175\5")
        buf.write("u;\2\u0173\u0175\n\4\2\2\u0174\u0171\3\2\2\2\u0174\u0172")
        buf.write("\3\2\2\2\u0174\u0173\3\2\2\2\u0175\u0178\3\2\2\2\u0176")
        buf.write("\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0179\3\2\2\2")
        buf.write("\u0178\u0176\3\2\2\2\u0179\u017a\7$\2\2\u017ax\3\2\2\2")
        buf.write("\u017b\u017c\7\62\2\2\u017c\u017e\t\5\2\2\u017d\u017f")
        buf.write("\t\6\2\2\u017e\u017d\3\2\2\2\u017f\u0180\3\2\2\2\u0180")
        buf.write("\u017e\3\2\2\2\u0180\u0181\3\2\2\2\u0181z\3\2\2\2\u0182")
        buf.write("\u0183\7\62\2\2\u0183\u0185\t\7\2\2\u0184\u0186\t\b\2")
        buf.write("\2\u0185\u0184\3\2\2\2\u0186\u0187\3\2\2\2\u0187\u0185")
        buf.write("\3\2\2\2\u0187\u0188\3\2\2\2\u0188|\3\2\2\2\u0189\u018b")
        buf.write("\t\2\2\2\u018a\u0189\3\2\2\2\u018b\u018e\3\2\2\2\u018c")
        buf.write("\u018a\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u0190\3\2\2\2")
        buf.write("\u018e\u018c\3\2\2\2\u018f\u0191\t\t\2\2\u0190\u018f\3")
        buf.write("\2\2\2\u0190\u0191\3\2\2\2\u0191\u0193\3\2\2\2\u0192\u0194")
        buf.write("\t\2\2\2\u0193\u0192\3\2\2\2\u0194\u0195\3\2\2\2\u0195")
        buf.write("\u0193\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u01a0\3\2\2\2")
        buf.write("\u0197\u0199\t\n\2\2\u0198\u019a\t\13\2\2\u0199\u0198")
        buf.write("\3\2\2\2\u0199\u019a\3\2\2\2\u019a\u019c\3\2\2\2\u019b")
        buf.write("\u019d\t\2\2\2\u019c\u019b\3\2\2\2\u019d\u019e\3\2\2\2")
        buf.write("\u019e\u019c\3\2\2\2\u019e\u019f\3\2\2\2\u019f\u01a1\3")
        buf.write("\2\2\2\u01a0\u0197\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1~")
        buf.write("\3\2\2\2\u01a2\u01a4\t\f\2\2\u01a3\u01a2\3\2\2\2\u01a4")
        buf.write("\u01a5\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2")
        buf.write("\u01a6\u01aa\3\2\2\2\u01a7\u01a9\t\r\2\2\u01a8\u01a7\3")
        buf.write("\2\2\2\u01a9\u01ac\3\2\2\2\u01aa\u01a8\3\2\2\2\u01aa\u01ab")
        buf.write("\3\2\2\2\u01ab\u0080\3\2\2\2\u01ac\u01aa\3\2\2\2\u01ad")
        buf.write("\u01af\t\16\2\2\u01ae\u01ad\3\2\2\2\u01af\u01b0\3\2\2")
        buf.write("\2\u01b0\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1\u01b2")
        buf.write("\3\2\2\2\u01b2\u01b3\bA\2\2\u01b3\u0082\3\2\2\2\u01b4")
        buf.write("\u01b5\7,\2\2\u01b5\u01b6\7,\2\2\u01b6\u01ba\3\2\2\2\u01b7")
        buf.write("\u01b9\13\2\2\2\u01b8\u01b7\3\2\2\2\u01b9\u01bc\3\2\2")
        buf.write("\2\u01ba\u01bb\3\2\2\2\u01ba\u01b8\3\2\2\2\u01bb\u01bd")
        buf.write("\3\2\2\2\u01bc\u01ba\3\2\2\2\u01bd\u01be\7,\2\2\u01be")
        buf.write("\u01bf\7,\2\2\u01bf\u01c0\3\2\2\2\u01c0\u01c1\bB\2\2\u01c1")
        buf.write("\u0084\3\2\2\2\u01c2\u01c3\13\2\2\2\u01c3\u0086\3\2\2")
        buf.write("\2\u01c4\u01c5\13\2\2\2\u01c5\u0088\3\2\2\2\u01c6\u01c7")
        buf.write("\13\2\2\2\u01c7\u008a\3\2\2\2\u01c8\u01c9\13\2\2\2\u01c9")
        buf.write("\u008c\3\2\2\2\22\2\u0168\u0174\u0176\u0180\u0187\u018c")
        buf.write("\u0190\u0195\u0199\u019e\u01a0\u01a5\u01aa\u01b0\u01ba")
        buf.write("\3\b\2\2")
        return buf.getvalue()


class BKITLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    SEMI = 1
    COLON = 2
    LP = 3
    RP = 4
    LB = 5
    RB = 6
    LSQ = 7
    RSQ = 8
    DOT = 9
    COMMA = 10
    ASS = 11
    INT_ADD = 12
    INT_MINUS = 13
    INT_MUL = 14
    MOD = 15
    INT_DIV = 16
    INT_EQ = 17
    INT_NEQ = 18
    INT_LESS_EQ = 19
    INT_GREAT_EQ = 20
    INT_LESS = 21
    INT_GREAT = 22
    FLOAT_ADD = 23
    FLOAT_MINUS = 24
    FLOAT_MUL = 25
    FLOAT_DIV = 26
    FLOAT_NEQ = 27
    FLOAT_GREAT = 28
    FLOAT_LESS = 29
    FLOAT_GREAT_EQ = 30
    FLOAT_LESS_EQ = 31
    NOT = 32
    AND_LOGIC = 33
    OR_LOGIC = 34
    VAR = 35
    BODY = 36
    ELSE = 37
    ENDFOR = 38
    IF = 39
    ENDDO = 40
    BREAK = 41
    ELSEIF = 42
    ENDWHILE = 43
    PARA = 44
    WHILE = 45
    CONITNUE = 46
    ENDBODY = 47
    FOR = 48
    RETURN = 49
    TRUE = 50
    FALSE = 51
    DO = 52
    ENDIF = 53
    FUNCTION = 54
    THEN = 55
    INTLIT = 56
    ESC_CHAR = 57
    SPEC_ESC = 58
    STRINGLIT = 59
    HEX = 60
    OCTAL = 61
    FLOATLIT = 62
    ID = 63
    WS = 64
    BLOCKCMT = 65
    ERROR_CHAR = 66
    UNCLOSE_STRING = 67
    ILLEGAL_ESCAPE = 68
    UNTERMINATED_COMMENT = 69

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "':'", "'('", "')'", "'{'", "'}'", "'['", "']'", "'.'", 
            "','", "'='", "'+'", "'-'", "'*'", "'%'", "'\\'", "'=='", "'!='", 
            "'<='", "'>='", "'<'", "'>'", "'+.'", "'-.'", "'*.'", "'\\.'", 
            "'=/='", "'>.'", "'<.'", "'>=.'", "'<=.'", "'!'", "'&&'", "'||'", 
            "'Var'", "'Body'", "'Else'", "'EndFor'", "'If'", "'EndDo'", 
            "'Break'", "'ElseIf'", "'EndWhile'", "'Parameter'", "'While'", 
            "'Continue'", "'EndBody'", "'For'", "'Return'", "'True'", "'False'", 
            "'Do'", "'EndIf'", "'Function'", "'Then'", "''\"'" ]

    symbolicNames = [ "<INVALID>",
            "SEMI", "COLON", "LP", "RP", "LB", "RB", "LSQ", "RSQ", "DOT", 
            "COMMA", "ASS", "INT_ADD", "INT_MINUS", "INT_MUL", "MOD", "INT_DIV", 
            "INT_EQ", "INT_NEQ", "INT_LESS_EQ", "INT_GREAT_EQ", "INT_LESS", 
            "INT_GREAT", "FLOAT_ADD", "FLOAT_MINUS", "FLOAT_MUL", "FLOAT_DIV", 
            "FLOAT_NEQ", "FLOAT_GREAT", "FLOAT_LESS", "FLOAT_GREAT_EQ", 
            "FLOAT_LESS_EQ", "NOT", "AND_LOGIC", "OR_LOGIC", "VAR", "BODY", 
            "ELSE", "ENDFOR", "IF", "ENDDO", "BREAK", "ELSEIF", "ENDWHILE", 
            "PARA", "WHILE", "CONITNUE", "ENDBODY", "FOR", "RETURN", "TRUE", 
            "FALSE", "DO", "ENDIF", "FUNCTION", "THEN", "INTLIT", "ESC_CHAR", 
            "SPEC_ESC", "STRINGLIT", "HEX", "OCTAL", "FLOATLIT", "ID", "WS", 
            "BLOCKCMT", "ERROR_CHAR", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", 
            "UNTERMINATED_COMMENT" ]

    ruleNames = [ "SEMI", "COLON", "LP", "RP", "LB", "RB", "LSQ", "RSQ", 
                  "DOT", "COMMA", "ASS", "INT_ADD", "INT_MINUS", "INT_MUL", 
                  "MOD", "INT_DIV", "INT_EQ", "INT_NEQ", "INT_LESS_EQ", 
                  "INT_GREAT_EQ", "INT_LESS", "INT_GREAT", "FLOAT_ADD", 
                  "FLOAT_MINUS", "FLOAT_MUL", "FLOAT_DIV", "FLOAT_NEQ", 
                  "FLOAT_GREAT", "FLOAT_LESS", "FLOAT_GREAT_EQ", "FLOAT_LESS_EQ", 
                  "NOT", "AND_LOGIC", "OR_LOGIC", "VAR", "BODY", "ELSE", 
                  "ENDFOR", "IF", "ENDDO", "BREAK", "ELSEIF", "ENDWHILE", 
                  "PARA", "WHILE", "CONITNUE", "ENDBODY", "FOR", "RETURN", 
                  "TRUE", "FALSE", "DO", "ENDIF", "FUNCTION", "THEN", "INTLIT", 
                  "ESC_CHAR", "SPEC_ESC", "STRINGLIT", "HEX", "OCTAL", "FLOATLIT", 
                  "ID", "WS", "BLOCKCMT", "ERROR_CHAR", "UNCLOSE_STRING", 
                  "ILLEGAL_ESCAPE", "UNTERMINATED_COMMENT" ]

    grammarFileName = "BKIT.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def emit(self):
        tk = self.type
        result = super().emit()
        if tk == self.UNCLOSE_STRING:
            raise UncloseString(result.text)
        elif tk == self.ILLEGAL_ESCAPE:
            raise IllegalEscape(result.text)
        elif tk == self.ERROR_CHAR:
            raise ErrorToken(result.text)
        elif tk == self.UNTERMINATED_COMMENT:
            raise UnterminatedComment()
        else:
            return result;


