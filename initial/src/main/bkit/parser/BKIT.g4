grammar BKIT;

@lexer::members {
def emit(self):
    tk = self.type
    result = super().emit()
    if tk == self.UNCLOSE_STRING:
        raise UncloseString(result.text)
    elif tk == self.ILLEGAL_ESCAPE:
        raise IllegalEscape(result.text)
    elif tk == self.ERROR_CHAR:
        raise ErrorToken(result.text)
    elif tk == self.UNTERMINATED_COMMENT:
        raise UnterminatedComment()
    else:
        return result;
}

options{
	language=Python3;
}

program         : declaration* EOF;
declaration     : varDec | funcDec ;

// declaration part
varDec          : VAR COLON id_list SEMI ;
funcDec         : FUNCTION COLON ident paralist? bodypart ;

// variable declaration
id_list         : id_form (COMMA id_form)* ;
id_form         : (ident | id_arr ) id_init?;                                              // identifiers declaration form
id_init         : '=' (prim_datatype | ident | array_type | call_expr) ;
id_arr          : ident index_op? ;                                                      // array dimension
array_type      : LB ( element_lit | element_com) RB;

// elements of literal and compound type
element_lit     : prim_datatype (COMMA prim_datatype)* ;
element_com     : subArr (COMMA subArr)*;
subArr          : LB element_com RB | LB element_lit RB ;

// expression
expr            : expr1 rel_sign expr1
                | expr1 ;

expr1           : expr1 (AND_LOGIC | OR_LOGIC) expr2
                | expr2 ;

expr2           : expr2 add_sub expr3
                | expr3 ;

expr3           : expr3 mul_div expr4
                | expr4 ;

expr4           : NOT expr4
                | expr5 ;

expr5           : minus expr5
                | expr6 ;

expr6           : (call_expr | ident | id_arr) index_op
                | expr7 ;

expr7           : LP expr RP
                | call_expr
                | operand ;
                                  
index_op        : '[' expr ']' | '[' expr ']' index_op ;

operand         : int_lit | float_lit | bool_lit | ident | array_type;

// function declaration
paralist        : PARA COLON para (COMMA para)* ;
para            : ident | id_arr ;
bodypart        : BODY COLON statement* ENDBODY DOT;
statement       : varDec | assign | if_stat | for_stat | while_stat | do_while | break_stat | cont_stat | call | ret_stat ;

// statement
assign          : assign_expr SEMI ;
assign_expr     : (ident index_op? | call_expr index_op)  '=' expr ;
if_stat         : IF expr THEN statement* (ELSEIF expr THEN statement*)* (ELSE statement*)? ENDIF DOT;
for_stat        : FOR LP ident '=' expr COMMA expr COMMA assign_expr RP DO statement* ENDFOR DOT;
while_stat      : WHILE LP (expr | bool_lit) RP DO statement* ENDWHILE DOT;
do_while        : DO statement* WHILE LP (expr | bool_lit) RP ENDDO DOT;
break_stat      : BREAK SEMI;
cont_stat       : CONITNUE SEMI;
call            : call_expr SEMI ;
call_expr       : ident LP expr (COMMA expr)* RP ;
ret_stat        : RETURN expr SEMI;


prim_datatype   : int_lit | float_lit | string_lit ;

int_lit         : INTLIT   ;
float_lit       : FLOATLIT ;
string_lit      : STRINGLIT;
bool_lit        : TRUE | FALSE ;
ident           : ID;

mul_div         : INT_MUL | FLOAT_MUL | INT_DIV | FLOAT_DIV | MOD ;
add_sub         : INT_ADD | FLOAT_ADD | minus ;
minus           : INT_MINUS | FLOAT_MINUS ;

equal           : INT_EQ ;
noteq           : INT_NEQ | FLOAT_NEQ ;
great           : INT_GREAT | FLOAT_GREAT | INT_GREAT_EQ | FLOAT_GREAT_EQ ;
less            : INT_LESS | FLOAT_LESS | INT_LESS_EQ | FLOAT_LESS_EQ;

rel_sign        : equal | noteq | great | less ;


// Tokens definition
// Separators
SEMI            : ';' ;
COLON           : ':' ;
LP              : '(' ;
RP              : ')' ;
LB              : '{' ;
RB              : '}' ;
LSQ             : '[' ;
RSQ             : ']' ;
DOT             : '.' ;
COMMA           : ',' ;

// Operators
// operators for INT
ASS             : '=';
INT_ADD         : '+';
INT_MINUS       : '-';
INT_MUL         : '*';
MOD             : '%';
INT_DIV         : '\\';

INT_EQ          : '==';
INT_NEQ         : '!=';
INT_LESS_EQ     : '<=';
INT_GREAT_EQ    : '>=';
INT_LESS        : '<';
INT_GREAT       : '>';

// operators for FLOAT
FLOAT_ADD       : '+.';
FLOAT_MINUS     : '-.';
FLOAT_MUL       : '*.';
FLOAT_DIV       : '\\.';

FLOAT_NEQ       : '=/=';
FLOAT_GREAT     : '>.';
FLOAT_LESS      : '<.';
FLOAT_GREAT_EQ  : '>=.';
FLOAT_LESS_EQ   : '<=.';

// logical operators
NOT             : '!';
AND_LOGIC       : '&&';
OR_LOGIC        : '||';



// Keywords
VAR             : 'Var' ;
BODY            : 'Body';
ELSE            : 'Else';
ENDFOR          : 'EndFor';
IF              : 'If';
ENDDO           : 'EndDo';
BREAK           : 'Break';
ELSEIF          : 'ElseIf';
ENDWHILE        : 'EndWhile';
PARA            : 'Parameter';
WHILE           : 'While';
CONITNUE        : 'Continue';
ENDBODY         : 'EndBody';
FOR             : 'For';
RETURN          : 'Return';
TRUE            : 'True';
FALSE           : 'False';
DO              : 'Do';
ENDIF           : 'EndIf';
FUNCTION        : 'Function';
THEN            : 'Then';


INTLIT          : [0-9]+;

ESC_CHAR        : '\\' ('b' | 'n' | 't' | 'f' | 'r'| '\\' | '\'') ;
SPEC_ESC        : '\'"' ;
STRINGLIT       : '"' ( ESC_CHAR | SPEC_ESC | ~('\\' | '"' | '\'') )* '"' ;

// '"' ('""' | '\'"' | ~('"' | '\\' | '\'') | '\\'[btnfr'\\] )*

HEX             : '0'[xX][0-9A-F]+;
OCTAL           : '0'[oO][0-7]+;

FLOATLIT        : [0-9]*[.]?[0-9]+([eE]([+-]?)[0-9]+)?;
ID              : [a-z]+[a-zA-Z0-9_]* ;
WS              : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines
BLOCKCMT        : ('**' .*? '**') -> skip;   // skip the comment section

ERROR_CHAR      : .;
UNCLOSE_STRING  : .;
ILLEGAL_ESCAPE  : .;
UNTERMINATED_COMMENT: .;